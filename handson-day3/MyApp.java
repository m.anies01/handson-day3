public class MyApp {
    public static void main(String[] args) {
        System.out.println("===== LAMPU =====");
        Posisi lampPos = new Lampu();
        PowerSetting pwL = new Lampu();
        lampPos.printPosisiLampu();
        pwL.powerOn();

        System.out.println("===== Handphone =====");
        MerkPhone nameMerk = new Phone();
        PowerSetting pwP = new Phone();
        nameMerk.printMerk();
        pwP.powerOff();
    }
}
