class Lampu implements PowerSetting,Posisi {
    boolean isPowerOn;
    String posisi = "Lampu depan";

    public void printPosisiLampu(){
        System.out.println(posisi);
    }
    public void powerOn() {
        isPowerOn = true;
        System.out.println("Dinyalakan....");    
    }
    public void powerOff() {
        isPowerOn = false;
        System.out.println("Dimatikan...");
    }    
}
